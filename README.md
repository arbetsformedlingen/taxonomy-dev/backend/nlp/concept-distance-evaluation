# concept-distance-evaluation

This repository is meant to store
1. A dataset for evaluating the quality of concept recommendations
2. Code to evaluate the quality of concept recommendations

The dataset is in the form of triplets of `anchor`, `positive` and `negative` concepts and the idea is that given the anchor concept, the positive example should have a higher recommendation than the negative concept. This is in the spirit of [Reimers, Nils, and Iryna Gurevych. "Sentence-bert: Sentence embeddings using siamese bert-networks." arXiv preprint arXiv:1908.10084 (2019).](https://arxiv.org/pdf/1908.10084.pdf), see section 3, **Triplet Objective Function**.

## Usage

## Building dataset

The datasets are stored under [datasets/](datasets/).

To generate new datasets, first call `make download-features` and then `make triplets-interactive`.

## Python API for evaluation

A Python API is provided for evaluation and it can be used in other projects by linking to the Git repo. If you are using Poetry, the line in `pyproject.toml` should be something like

```toml
concept_distance_evaluation = { git = "https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/concept-distance-evaluation.git", branch = "master" }
```
If you are instead using a `requirements.txt` file, it should probably be something like
```
git+https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/concept-distance-evaluation.git@master#egg=concept_distance_evaluation
```
Read more about using Git dependencies from Python [here](https://stackoverflow.com/a/43261155/1008794).

If you want to evaluate an embedding algorithm, that maps a string to a vector, you use the `evaluate_concept_embedder` function. Here is an example:

```py
import concept_distance_evaluation.core as cde

def my_embedding_algorithm(concept):
    id = concept["id"]
    label = concept["label"]

    if label == "Snickare":
        return [0, 0, 1]
    elif label == "Hantverkare":
        return [0, 0.2, 0.5]
    else:
        return [0, 0, 0]

dataset = cde.load_datasets(cde.standard_dataset_paths)
results = cde.evaluate_concept_embedder(dataset, my_embedding_algorithm)
summary = summarize(results)

summarize(results)
# => {'overall': {'total_count': 1046, 'correct_count': 484}, 'per_dataset': {'occupation-name-0': {'total_count': 1046, 'correct_count': 484}}}
```
This will first embed all concepts using the supplied function, then classify the dataset triplets based on the `L2` distance of the embedder function.

If you instead want to classify the triplets directly, that can be done using the `evaluate_triplets` function:
```py
import concept_distance_evaluation.core as cde

def my_classifier(anchor, alternatives):
    for x in alternatives:
        if x["label"] == anchor["label"]:
            return x["id"]
    return alternatives[0]["id"]

dataset = cde.load_datasets(cde.standard_dataset_paths)
results = cde.evaluate_triplets(dataset, my_classifier)
summary = summarize(results)

summarize(results)
# => {'overall': {'total_count': 1046, 'correct_count': 513}, 'per_dataset': {'occupation-name-0': {'total_count': 1046, 'correct_count': 513}}}
```

## License

Copyright © 2022 Arbetsförmedlingen JobTech

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
