(ns concept-distance-evaluation.core-test
  (:import [java.nio.file Files]
           [java.io File])
  (:require [clojure.test :refer :all]
            [concept-distance-evaluation.core :refer :all]))

(def sample-triplet-data {:feature
                          {:path
                           [:occupation-name
                            :broader
                            :ssyk-level-4
                            :broader
                            :occupation-field]},
                          :data
                          [{:anchor {:id "tPp6_tmB_EsY", :preferred-label "Uthyrningschef"},
                            :positive
                            {:id "zD7L_ZqX_cqz", :preferred-label "Förlagschef bokförlag"},
                            :negative
                            {:id "gBUP_Hiy_eaH",
                             :preferred-label "Plattsättare utan yrkesbevis"},
                            :common
                            {:id "bh3H_Y3h_5eD",
                             :preferred-label "Chefer och verksamhetsledare"},
                            :created-timestamp "",
                            :status nil
                            :method nil
                            :corrupt? true}
                           {:anchor {:id "jUbF_gem_LC8", :preferred-label "Bildpedagog"},
                            :positive {:id "xrVh_3uf_kWi", :preferred-label "Mimpedagog"},
                            :negative
                            {:id "k7GJ_Ysw_GbY", :preferred-label "Turistintendent"},
                            :common {:id "MVqp_eS8_kDZ", :preferred-label "Pedagogik"},
                            :created-timestamp "",
                            :status nil
                            :method nil                            
                            :corrupt? true}
                           {:anchor
                            {:id "kVwP_Ej8_AcE",
                             :preferred-label "Djurskötare djuruppfödning"},
                            :positive
                            {:id "QZSd_fkK_nNA", :preferred-label "Pälsdjursuppfödare"},
                            :negative {:id "8Hsw_57N_RvG", :preferred-label "Sufflör"},
                            :common {:id "VuuL_7CH_adj", :preferred-label "Naturbruk"},
                            :created-timestamp "",
                            :status nil
                            :method nil
                            :corrupt? false}]})

(deftest triplet-test
  (let [sample-filename (-> "testdata"
                            (File/createTempFile ".txt")
                            str)
        prefix (subs sample-filename 0 (- (count sample-filename) 4))]
    (save-triplet-data prefix sample-triplet-data)
    (is (= sample-triplet-data (load-triplet-data prefix))))
  (let [merged (merge-triplets sample-triplet-data
                               sample-triplet-data)]
    (is (= 6 (-> merged :data count)))
    (is (= 3 (-> sample-triplet-data :data count)))
    (is (= #{:feature :data} (-> merged keys set)))))

(deftest evaluate-batch-test
  (let [triplets (load-all-triplets (:dataset-dir default-config))
        results (evaluate triplets (constantly true))
        correct-triplet-set (into #{} (comp (remove :corrupt?) (map #(dissoc % :corrupt?))) triplets)
        all-corr (evaluate triplets (comp boolean correct-triplet-set))
        all-wrong (evaluate triplets (comp not boolean correct-triplet-set))]
    (is (< 0 (:correct-rate results)))
    (is (< (:correct-rate results) 1))
    (is (= 1 (:correct-rate all-corr)))
    (is (= 0 (:correct-rate all-wrong)))))
