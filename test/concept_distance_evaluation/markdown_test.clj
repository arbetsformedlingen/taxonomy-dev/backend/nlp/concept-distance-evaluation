(ns concept-distance-evaluation.markdown-test
  (:require [clojure.test :refer :all]
            [concept-distance-evaluation.markdown :refer :all]
            [clojure.string :as cljstr]))

(deftest markdown-test
  (is (= (collapse-whitespace [\space (newlines 3) \newline])
         [\space \newline \newline \newline]))
  (is (= (combine-specials [\a \b \space whitespace \c])
         [\a \b \space \c]))
  (is (= (combine-specials [\a \b whitespace \c])
         [\a \b \space \c]))
  (is (= (combine-specials [\a \b whitespace \newline \c])
         [\a \b \newline \c]))
  (is (= (combine-specials [\a \b \space whitespace (newlines 2) \c])
         [\a \b \space \newline \newline \c]))
  (is (= (normalize-sub [] [:a [:b [[["Mjao"]]]]])
         [[:a [:b [:text "Mjao"]]]]))
  (is (= (normalize [:a [:b [[["Mjao"]]]]])
         [:seq [:a [:b [:text "Mjao"]]]]))
  (is (= (render [:section "Mjao" [:section "Mu"]])
         "# Mjao\n\n## Mu"))
  (is (= (render [:bold "abc"])
         "**abc**"))
  (is (= (render [:bold "abc" "def"])
         "**abc** **def**"))
  (is (= (render [:table 
                  [[:row "Key" [[["Value"]]]]
                   [[[[[:bar]]]]]] 
                  [:row "a" "119"]])
         "| Key | Value |\n|-|-|\n| a | 119 |"))
  (is (= (render [:bold "a" [:italic "b"] "c"])
         "**a** ***b*** **c**")))
