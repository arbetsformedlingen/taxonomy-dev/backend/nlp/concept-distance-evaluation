(ns concept-distance-evaluation.core
  (:require [clojure.spec.alpha :as spec]
            [cheshire.core :as cheshire]
            [clojure.java.io :as io]
            [clojure.spec.alpha :as spec]
            [clojure.string :as cljstr]
            [clojure.set :as cljset]
            [clj-http.client :as client]
            [concept-distance-evaluation.markdown :as md]
            [clojure.edn :as edn]
            [clojure.data.csv :as csv])
  (:import [java.text SimpleDateFormat]
           [java.util TimeZone Date]))

(spec/def :feature-def/path (spec/cat :start-type keyword?
                                      :path (spec/* (spec/cat :relation keyword?
                                                              :next-type keyword?))))
(spec/def :feature-def/feature (spec/keys :req-un [:feature-def/path]))
(defn conform-feature [feature]
  (spec/conform :feature-def/feature feature))

(spec/def ::keyword-as-string (spec/conformer (fn [x] (cond
                                                        (nil? x) nil
                                                        (string? x)
                                                        (let [x (cljstr/trim x)]
                                                          (if (empty? x)
                                                            nil
                                                            (keyword x)))
                                                        :default ::spec/invalid))
                                              (fn [kwd]
                                                (cond
                                                  (nil? kwd) ""
                                                  (keyword? kwd) (name kwd)
                                                  :default (str kwd)))))

(spec/def ::int-as-string (spec/conformer (fn [x]
                                            (try
                                              (Long/parseLong x)
                                              (catch Exception e
                                                ::spec/invalid)))
                                          str))

(spec/def :triplet-data/id string?)
(spec/def :triplet-data/preferred-label string?)
(spec/def :triplet-data/concept (spec/keys :req-un [:triplet-data/id :triplet-data/preferred-label]))
(spec/def :triplet-data/positive :triplet-data/concept)
(spec/def :triplet-data/negative :triplet-data/concept)
(spec/def :triplet-data/anchor :triplet-data/concept)
(spec/def :triplet-data/triplet (spec/keys :req-un [:triplet-data/positive
                                                    :triplet-data/negative
                                                    :triplet-data/anchor]))
(spec/def :triplet-data/data (spec/coll-of :triplet-data/triplet))
(spec/def :triplet-data/triplet-data (spec/keys :req-un [:triplet-data/data :feature-def/feature]))


(def features
  {:occupation-name-0 {:path [:occupation-name :broader :ssyk-level-4 :broader :occupation-field]}
   :occupation-name-1 {:path [:occupation-name :broader :ssyk-level-4]}})

(defn feature-name [{:keys [path]}]
  (cljstr/join "__" (map name path)))

(def default-config {:address "https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql"
                     :retry-count 4
                     :features features
                     :work-dir "work"
                     :dataset-dir "datasets"})

;; query MyQuery {
;;   concepts(type: "occupation-name") {
;;     id
;;     broader(type: "ssyk-level-4") {
;;       id
;;       broader(type: "occupation-field") {
;;         id
;;         preferred_label
;;       }
;;     }
;;     preferred_label
;;   }
;; }


(defn quoted-key [s]
  (str "\"" (name s) "\""))

(defn render-feature-query [feature]
  (let [{:keys [start-type path]} (:path feature)]
    (str "query MyQuery { concepts(type: " (quoted-key start-type) ") {id preferred_label "
         (reduce (fn [expr {:keys [relation next-type]}]
                   (str (name relation)
                        "(type: "
                        (quoted-key next-type)
                        ") {id "
                        "preferred_label " #_(if (= expr "") "preferred_label " "")
                        expr "}"))
                 ""
                 (reverse path))
         "}}")))

(defn retry-loop [request-fn result-fn retry-count]
  (loop [responses []]
    (if (= (count responses) retry-count)
      [:failure responses]
      (let [response (request-fn)]
        (if (= 200 (:status response))
          [:success (result-fn response)]
          (do (println (format "   WARNING: Query failed (%s), retrying" (str (:status response))))
              (recur (conj responses response))))))))

(defn normalize-string-keys [m]
  (into {} (for [[k v] m]
             [(cljstr/replace k "_" "-") v])))

(defn concept-sequence-from-graphql-response [{:keys [path]} element]
  (reduce (fn [result {:keys [relation]}]
            (let [relation (name relation)]
              (for [dst result
                    y (get (last dst) relation)]
                (into (subvec dst 0 (dec (count dst)))
                      [(dissoc (last dst) relation) y]))))
          [[element]]
          (:path path)))

(defn download-feature [{:keys [address retry-count]} conformed-feature]
  (let [query (render-feature-query conformed-feature)]
    (retry-loop #(try
                   (client/get
                    address
                    {:accept :json :query-params {"query" query}})
                   (catch Exception e
                     {:exception e
                      :status :exception}))
                #(into []
                       (comp (mapcat (partial concept-sequence-from-graphql-response conformed-feature))
                             (map (partial mapv normalize-string-keys)))
                       (-> %
                           :body
                           cheshire/parse-string
                           (get-in ["data" "concepts"])))
                retry-count)))

(comment
  (do

    (def f (->> (download-feature default-config (-> features first conform-feature))
                second
                shuffle
                (take 20)))
    

    ))

(defn features-file [config feature-key]
  (io/file (:work-dir config) (str (name feature-key) ".json")))

(defn fix-feature-path [x]
  (update-in x [:feature :path] (partial mapv keyword)))

(defn load-features [file]
  (if (.exists file)
    (-> file
        slurp
        (cheshire/parse-string true)
        fix-feature-path)))

(defn build-groups [features]
  (into {}
        (remove #(= 1 (count (second %))))
        (reduce (fn [dst path]
                  (let [leaf (first path)
                        top (last path)]
                    (update dst (:id top) (fn [group] (conj (or group #{}) (:id leaf))))))
                {}
                (:data features))))

(defn build-memberships [groups features]
  (reduce (fn [dst path]
            (let [leaf (first path)
                  top-id (:id (last path))
                  id (:id leaf)]
              (if (groups top-id)
                (update dst id (fn [groups] (conj (or groups #{}) top-id)))
                dst)))
          {}
          (:data features)))

(defn build-concept-info [features]
  (into {} (comp cat (map (juxt :id identity))) (:data features)))

(defn generate-triplet [memberships groups]
  (let [candidates (keys memberships)
        anchor (rand-nth candidates)
        group-keys (get memberships anchor)
        common (->> group-keys
                    seq
                    rand-nth)
        positives (->> common
                       (get groups)
                       (remove #{anchor}))]
    (assert (seq positives))
    (let [positive (rand-nth positives)
          negative (loop []
                     (let [candidate (rand-nth candidates)]
                       (if (empty? (cljset/intersection group-keys (get memberships candidate)))
                         candidate
                         (recur))))]
      {:anchor anchor
       :positive positive
       :negative negative
       :common common
       })))

(defn- local-date-format-from-string [src]
  (doto (SimpleDateFormat. src)
    (.setTimeZone (TimeZone/getTimeZone "UTC"))))

(def date-format (local-date-format-from-string "yyyy-MM-dd'T'HH:mm'Z'"))

(defn get-timestamp []
  (.format date-format (Date.)))

(defn randomly-mark-corrupt [x]
  (merge {:corrupt? (zero? (rand-int 2))} x))

(defn initialize-base-triplet-data []
  {:created-timestamp (get-timestamp)
   :status nil
   :method nil})

(defn triplet? [x]
  (and (map? x)
       (every? (partial contains? x) [:positive :negative :anchor])))

(defn get-user-from-config [config]
  (if-let [user (:user config)]
    user
    (loop []
      (println "Hint: Define the JOBTECH_USER environment variable to avoid this question.")
      (println "JobTech user name (e.g. on Mattermost)?")
      (let [answer (cljstr/trim (read-line))]
        (if (empty? answer)
          (recur)
          answer)))))

(defn generate-triplets-loop [user features process-fn]
  (let [base (assoc (initialize-base-triplet-data)
                    :created-by user)
        groups (build-groups features)
        memberships (build-memberships groups features)
        cinfo (build-concept-info features)
        wrap (fn [triplets] {:feature (:feature features)
                             :data triplets})]
    (loop [result []]
      (let [new-triplet (process-fn
                         (wrap result)
                         (into
                          (randomly-mark-corrupt base)
                          (for [[k v] (generate-triplet memberships groups)]
                            [k (get cinfo v)])))]
        (cond
          (nil? new-triplet) (wrap result)
          (= :ignore new-triplet) (recur result)
          (triplet? new-triplet) (recur (conj result new-triplet))
          :default (throw (ex-info "Invalid return value" {:triplet new-triplet})))))))

(defn triplet-files [file-prefix]
  (for [suf [".csv" ".json"]]
    (io/file (str file-prefix suf))))

(defn string-set-conformer [value-map]
  (let [inv-map (into {} (for [[k vs] value-map
                               v vs]
                           [v k]))]
    (spec/conformer (fn [x]
                      (if (string? x)
                        (get inv-map (-> x cljstr/trim cljstr/lower-case) ::spec/invalid)
                        ::spec/invalid))
                    (fn [x]
                      (if-let [ss (get value-map x)]
                        (first ss)
                        "")))))
(spec/def ::bool-as-string (string-set-conformer {true ["1" "true" "yes"]
                                                  false ["0" "false" "no" ""]}))

(spec/def ::status (string-set-conformer {:good ["good" "1"]
                                          :bad ["bad" "0"]
                                          :unsure ["?" "unsure"]
                                          nil [""]}))

(defn remove-special-csv-chars [x]
  (cond
    (nil? x) ""
    (string? x) (-> x
                    (cljstr/replace "," "")
                    (cljstr/replace "\n" ""))
    :default (throw (ex-info "Cannot unform" {:value x}))))

(spec/def ::csv-string (spec/conformer #(if (string? %) % ::spec/invalid)
                                       remove-special-csv-chars))

(def columns [["corrupt?" [:corrupt?] ::bool-as-string]
              ["common-label" [:common :preferred-label]]
              ["anchor-label" [:anchor :preferred-label]]
              ["positive-label" [:positive :preferred-label]]
              ["negative-label" [:negative :preferred-label]]
              
              ["common-id" [:common :id]]
              ["anchor-id" [:anchor :id]]
              ["positive-id" [:positive :id]]
              ["negative-id" [:negative :id]]
              
              ["method" [:method] ::keyword-as-string]
              ["created-by" [:created-by]]
              ["created-timestamp" [:created-timestamp]]
              ["status" [:status] ::status]])

(defn export-triplet [triplet]
  (into [] (for [[_ path conf] columns]
             (spec/unform (or conf ::csv-string) (get-in triplet path)))))

(defn save-triplet-data [file-prefix triplet-data]
  (let [metadata (dissoc triplet-data :data)
        triplets (:data triplet-data)
        [data-file meta-file] (triplet-files file-prefix)]
    (io/make-parents data-file)
    (spit meta-file (cheshire/generate-string metadata))
    (with-open [writer (io/writer data-file)]
      (csv/write-csv writer
                     (into [(mapv first columns)]
                           (map export-triplet)
                           triplets)))))

(defn load-triplet-data
  "Given a path prefix (the filename except endings .json and .csv), load all data from the file"
  [file-prefix]
  (let [[data-file meta-file] (triplet-files file-prefix)]
    (when (and (.exists data-file)
               (.exists meta-file))
      (let [metadata (-> meta-file
                         slurp
                         (cheshire/parse-string true)
                         fix-feature-path)
            [header & data] (with-open [reader (io/reader data-file)]
                              (doall
                               (csv/read-csv reader)))
            column-map (into {} (map (juxt first rest)) columns)
            header-columns (map column-map header)]
        (assoc metadata :data (for [row data]
                                (reduce (fn [dst [[path conf] v]]
                                          (assoc-in dst path (spec/conform (or conf ::csv-string) v)))
                                        {}
                                        (map vector header-columns row))))))))

(defn split-filename [filename]
  (if-let [i (cljstr/last-index-of filename \.)]
    [(subs filename 0 i) (subs filename i)]))

(defn triplets-with-feature [{:keys [data feature]}]
  (mapv #(assoc % :feature feature) data))

(defn filter-by-status [status-pred]
  (filter (comp status-pred :status)))

(defn load-all-triplets [dataset-dir]
  (->> dataset-dir
       io/file
       file-seq
       (map #(split-filename (.getAbsolutePath %)))
       (remove nil?)
       (group-by first)
       
       (into
        []
        (comp (filter
               #(= #{".json" ".csv"}
                   (into #{} (map (comp cljstr/lower-case second)) (second %))))
              (map (comp triplets-with-feature load-triplet-data first))
              cat))))

(defn download-features [config]
  (doseq [[k feature] (:features config)]
    (let [file (features-file config k)
          [status data] (download-feature config (conform-feature feature))
          all-data {:feature feature
                    :data data}]
      (if (= :success status)
        (do 
          (io/make-parents file)
          (spit file (cheshire/generate-string all-data))
          (println (format "Saved %d paths to %s" (count data) (.getAbsolutePath file))))
        (println (format "Failed to download feature %s" (str (:key feature))))))))

(defn merge-triplets [a b]
  (cond
    (nil? a) b
    (nil? b) a
    :default 
    (let [af (:feature a)
          bf (:feature b)]
      (when-not (= af bf)
        (throw (ex-info "Cannot mix different features in one dataset"
                        {:a af :b bf})))
      (update a :data #(into (vec %) (:data b))))))

(defn sort-triplets [triplets]
  (->> triplets
       (map vector (range))
       (sort-by (fn [[i triplet]]
                  [(case (:status triplet)
                     :good 0
                     :unsure 1
                     nil 2
                     :bad 3) i]))
       (map second)))

(defn sort-triplet-data [triplet-data]
  (update triplet-data :data sort-triplets))

(defn swap-positive-negative [triplet]
  (merge triplet {:positive (:negative triplet)
                  :negative (:positive triplet)}))

(defn apply-corruption [triplet]
  (dissoc (if (:corrupt? triplet)
            (swap-positive-negative triplet)
            triplet)
          :corrupt?))

(defn evaluate-batch [triplets batch-fn]
  {:pre [(spec/valid? :triplet-data/data triplets)]}
  (let [corrupeted-triplets (map apply-corruption triplets)
        correct-bools (batch-fn corrupeted-triplets)
        n (count triplets)
        _ (assert (= n (count correct-bools)))
        _ (assert (every? boolean? correct-bools))
        results (mapv (fn [triplet correct?]
                        (assoc triplet :correct? (= correct? (not (:corrupt? triplet)))))
                      triplets
                      correct-bools)
        correct-count (count (filter :correct? results))]
    {:triplets results
     :total-count n
     :correct-count correct-count
     :correct-rate (/ correct-count n)}))

(defn evaluate [triplets evalfn]
  (evaluate-batch triplets #(map evalfn %)))

(defn initiate-triplet-generation [config params]
  (let [{:keys [features-key output-prefix]} params
        previous-triplets (load-triplet-data output-prefix)
        features (load-features (features-file config features-key))]
    [features
     (fn [new-triplets]
       (let [all-triplets (merge-triplets previous-triplets new-triplets)]
         (save-triplet-data output-prefix all-triplets)
         all-triplets))]))

(defn api-generate-triplets [config params]
  (let [[features save-triplets-fn] (initiate-triplet-generation config params)
        n (:count params)]
    (save-triplets-fn
     (generate-triplets-loop
      (get-user-from-config config)
      features (fn [result new-triplet]
                 (when (-> result :data count (< n))
                   new-triplet))))))

(defn full-concept-label [concept]
  (format "%s (%s)" (:preferred-label concept) (:id concept)))

(defn render-choice [key triplet]
  (println (str "  (" key ") * " (full-concept-label (:anchor triplet))))
  (println (str "      * " (full-concept-label (:positive triplet)))))

(defn api-generate-triplets-interactive [config params]
  (let [[features save-triplets-fn] (initiate-triplet-generation config params)]
    (save-triplets-fn
     (generate-triplets-loop
      (get-user-from-config config)
      features
      (fn [result triplet]

        ;; Always save, in case the user presses Ctrl + C.
        (save-triplets-fn result)
        
        (let [new-base {:method :interactive
                        :created-timestamp (get-timestamp)}
              triplet (merge triplet new-base)

              ;; This is the triplet suggested based on
              ;; a common parent concept. If the user chooses this triplet,
              ;; the status is marked as "good".
              suggestion (assoc triplet :status :good)

              ;; This is a triplet *opposite* of the suggestion. If this triplet
              ;; is chosen, the status is marked as "unsure" in case we want
              ;; to review it later on.
              opposite (assoc (swap-positive-negative triplet) :status :unsure)
              
              [choice-a choice-b] (shuffle [suggestion opposite])]
          (println "\n\nGenerated" (-> result :data count) "triplets so far.")
          (println "\n******* Which pair of concepts is closest?\n")
          (render-choice "a" choice-a)
          (println "\n    or \n")
          (render-choice "b" choice-b)
          (println "\n  (q) Quit")
          (println "\n  Other key: Don't know. Skip this one.")
          (println "\nHint: " (full-concept-label (:common triplet)))
          (print "Answer?")
          (case (-> (read-line) cljstr/trim cljstr/lower-case)
            "a" choice-a
            "b" choice-b
            "q" nil
            :ignore)))))))

(defn api-normalize [config {:keys [path-prefix]}]
  (when-let [tdata (load-triplet-data path-prefix)]
    (save-triplet-data path-prefix (-> tdata sort-triplet-data))))

(defn process-command [config [command-name command-args]]
  (or (case command-name
        :download-features (download-features config)
        :generate-triplets (api-generate-triplets config command-args)
        :generate-triplets-interactive (api-generate-triplets-interactive config command-args)
        :normalize (api-normalize config command-args))
      config))

(spec/def ::cmd (spec/alt :download-features #{"download-features"}
                          :generate-triplets (spec/cat :prefix #{"generate-triplets"}
                                                       :features-key ::keyword-as-string
                                                       :count ::int-as-string
                                                       :output-prefix string?)
                          :generate-triplets-interactive (spec/cat :prefix #{"generate-triplets-interactive"}
                                                                   :features-key ::keyword-as-string
                                                                   :output-prefix string?)
                          :normalize (spec/cat :prefix #{"normalize"}
                                               :path-prefix string?)))
(spec/def ::args (spec/* ::cmd))

(defn get-user-from-env []
  (if-let [user (get (System/getenv) "JOBTECH_USER" nil)]
    {:user user}
    {}))

(defn -main [& args]
  (let [commands (spec/conform ::args args)]
    (if (= commands ::spec/invalid)
      (println "Invalid arguments:" args)
      (reduce process-command (merge default-config (get-user-from-env)) commands))))

(comment
  (do

    (def triplets (load-all-triplets (:dataset-dir default-config)))

    ))

(comment
  (do

    (def out (-main "generate-triplets" "occupation-name-0" "10" "/tmp/kattskit"))

    ))

(comment
  (do

    (def out (-main "generate-triplets-interactive" "occupation-name-0" "/tmp/kattskit"))

    ))

(comment
  (do


    (def triplets (load-triplet-data "datasets/occupation-name-0"))

    ))

(comment
  (do

    (def fdata (load-features (features-file default-config :occupation-name-0)))
    (def groups (build-groups fdata))
    (def memberships (build-memberships groups fdata))
    (def cinfo (build-concept-info fdata))
    (def triplet (generate-triplet memberships groups))

    (def triplet-data (generate-triplets-data fdata 100))
    (save-triplet-data "/tmp/testdata" triplet-data)

    (def d2 (load-triplet-data "/tmp/testdata"))
    
    ))
