(ns concept-distance-evaluation.markdown
  (:require [clojure.spec.alpha :as spec]
            [clojure.string :as cljstr]
            [clojure.set :as cljset]))

(declare render-sub)

(def node-types #{:section :bold :italic})

(defn normalize-sub [result x]
  (cond
    (string? x) (conj result [:text x])
    (and (sequential? x) (keyword? (first x))) (conj result (reduce normalize-sub [(first x)] (rest x)))
    (sequential? x) (reduce normalize-sub result x)
    :default (throw (ex-info "Cannot normalize"
                             {:result result
                              :x x}))))

(def normalize (partial normalize-sub [:seq]))

(def init-context {:section-depth 0
                   :text-features #{}
                   :text []})

(defn restore-context [current-context previous-context]
  (merge current-context (dissoc previous-context :text)))

(defn write [c & args]
  (reduce
   (fn [c s]
     (cond
       (string? s) (update c :text into s)
       :default (update c :text conj s)))
   c args))

(defn combine-special [a b]
  {:pre [(map? a)
         (map? b)]}
  (merge-with max a b))

(defn whitespace-char? [x]
  (and (char? x)
       (Character/isWhitespace x)))

(defn whitespace? [x]
  (or (whitespace-char? x)
      (map? x)))


(defn newlines [n]
  {:newlines n
   :whitespace 0})

(def whitespace
  {:newlines 0
   :whitespace 1})

(defn collapse-whitespace [ws]
  (let [[chars specials] (split-with
                          whitespace-char?
                          (map second
                               (sort-by
                                (fn [[i x]] [(if (whitespace-char? x) 0 1) i])
                                (map vector (range) ws))))
        chars (vec chars)
        special (reduce combine-special (newlines 0) specials)
        target-nl (:newlines special)]
    (cond
      (= special (newlines 0)) chars
      (zero? target-nl) (if (empty? chars)
                          [\space]
                          chars)
      :default (let [nl (count (filter #{\newline} chars))]
                 (into chars (repeat (max 0 (- target-nl nl)) \newline))))))

(defn combine-specials [text]
  (into []
        (comp (partition-by whitespace?)
              (mapcat (fn [part]
                        (if (whitespace? (first part))
                          (collapse-whitespace part)
                          part))))
        text))

(defn context-str [c]
  (->> c
       :text
       combine-specials
       (reduce (fn [dst x]
                 (cond
                   (char? x) (conj dst x)
                   (map? x) (let [{:keys [newlines whitespace]} x]
                              (into dst (if (zero? newlines)
                                          (repeat whitespace \space)
                                          (repeat newlines \newline))))
                   :default (throw (ex-info "Cannot render to string" {:element x}))))
               [])
       (apply str)
       cljstr/trim))

(defn render-seq [context data]
  (reduce render-sub context data))

(defn render-section [context [header & children]]
  (-> context
      (write (newlines 2) (apply str (repeat (inc (:section-depth context)) "#")) " ")
      (render-sub header)
      (write (newlines 1))
      (update :section-depth inc)
      (render-seq children)
      (restore-context context)))

(defn formatting-str [features]
  (if (seq features)
    (let [stylestr (apply str (repeat (transduce
                                       (map #(case % :bold 2 :italic 1 0))
                                       + 0 features)
                                      "*"))]
      (str stylestr (if (features :tt) "`" "")))))

(defn write-formatted [dst s]
  {:pre [(string? s)]}
  (if-let [fmt (-> dst :text-features formatting-str)]
    (write dst whitespace fmt s (cljstr/reverse fmt) whitespace)
    (write dst s)))

(defn render-text [context data]
  (reduce write-formatted context data))

(defn text-feature [feature]
  (fn [context children]
    (-> context
        (update :text-features conj feature)
        (render-seq children)
        (restore-context context))))

(defn render-rows [context rows]
  (let [row-lengths (into #{}
                          (comp (filter (comp #{:row} first))
                                (map (comp count rest)))
                          rows)
        _ (assert (= 1 (count row-lengths)))
        columns (first row-lengths)]
    (reduce (fn this [context [k & args]]
              (write (case k
                       :bar (write context (apply str "|" (repeat columns "-|")))
                       :row (reduce (fn [context arg]
                                      (-> context
                                          (write whitespace)
                                          (render-sub arg)
                                          (write whitespace)
                                          (write "|")))
                                    (write context "|")
                                    args))
                     "\n"))
            context
            rows)))

(defn render-table [context rows]
  (-> context
      (write (newlines 2))
      (render-rows rows)
      (write (newlines 2))))

(defn render-sub [dst [k & data]]
  ((case k
     :section render-section
     :seq render-seq
     :text render-text
     :bold (text-feature :bold)
     :italic (text-feature :italic)
     :tt (text-feature :tt)
     :table render-table
     (throw (ex-info "Cannot render"
                     {:type k
                      :args data}))) dst data))

(defn render [expr]
  (->> expr
       normalize
       (render-sub init-context)
       context-str))
