import numpy as np
import csv
import os
from cached_path import cached_path

columns = {'common-label': ["common", "label"],
           'anchor-label': ["anchor", "label"],
           'positive-label': ["positive", "label"],
           'negative-label': ["negative", "label"],
           'common-id': ["common", "id"],
           'anchor-id': ["anchor", "id"],
           'positive-id': ["positive", "id"],
           'negative-id': ["negative", "id"]}

def set_in(dst, path, v):
    if len(path) < 1:
        raise RuntimeError("Path must be at least 1")
    k = path[0]
    r = path[1:]
    if len(path) == 1:
        dst[k] = v
    else:
        if not(k in dst):
            dst[k] = {}
        set_in(dst[k], r, v)

def dataset_name_from_path(csv_path):
    (directory, only_filename) = os.path.split(csv_path)
    (dataset_name, ext) = os.path.splitext(only_filename)
    return dataset_name

def parse_bool(x):
    true_strings = {"true", "1", "yes", "ja", "sant"}
    return x.lower() in true_strings
        

def load_dataset_into(ds_path, output_list):
    dataset_name = dataset_name_from_path(ds_path)
    with open(cached_path(ds_path), encoding="utf-8") as f:
        reader = csv.reader(f, delimiter=',')
        data = []
        header = None
        for row in reader:
            if header == None:
                header = row
            else:
                assert(len(row) == len(header))
                dst = {"dataset-name": dataset_name}
                for (h, v) in zip(header, row):
                    path = columns.get(h, [h])
                    set_in(dst, path, v)
                dst["corrupt?"] = parse_bool(dst["corrupt?"])
                output_list.append(dst)

def flatten_rec(dst, s):
    if hasattr(s, "__iter__") and not(isinstance(s, str)):
        for x in s:
            flatten_rec(dst, x)
    else:
        dst.append(s)
    return dst

def evaluate_triplet(sample, evaluator_fn):
    pos = sample["positive"]
    neg = sample["negative"]
    alternatives = [pos, neg]
    if sample["corrupt?"]:
        alternatives.reverse()

    id_of_closest = evaluator_fn(sample["anchor"], alternatives)
    assert(id_of_closest in set([pos["id"], neg["id"]]))
    return {**sample, "correctly-classified?": id_of_closest == pos["id"]}
    
##### API

standard_dataset_paths = ["https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/concept-distance-evaluation/-/raw/master/datasets/occupation-name-0.csv"]

def load_datasets(*paths):
    dst = []
    for path in flatten_rec([], paths):
        load_dataset_into(path, dst)
    return dst

def evaluate_triplets(dataset, evaluator_fn):
    return [evaluate_triplet(x, evaluator_fn) for x in dataset]

def evaluate_concept_embedder(dataset, embedder_fn):
    all_concepts = [sample[k] for sample in dataset for k in ["positive", "anchor", "negative"]]
    concept_vector_map = {}
    n = None
    for c in all_concepts:
        cid = c["id"]
        if not(cid in concept_vector_map):
            v = np.array(embedder_fn(c))
            (dim,) = v.shape
            if n == None:
                n = dim
            elif n != dim:
                raise RuntimeError("Inconsistent embedding vector dimension. Previously got {:d}, now it is {:d}.".format(n, dim))
            
            concept_vector_map[cid] = v

    def evec(concept):
        return concept_vector_map[concept["id"]]
            
    def triplet_evaluator(anchor, alternatives):
        best = None
        for alt in alternatives:
            cand = (np.linalg.norm(evec(anchor) - evec(alt)), alt["id"])
            if best == None:
                best = cand
            else:
                best = min(best, cand)
        return best[1]

    return evaluate_triplets(dataset, triplet_evaluator)

def summarize(dataset_results):
    def init():
        return {"total_count": 0, "correct_count": 0}
    
    overall = init()
    per_dataset = {}

    for x in dataset_results:
        name = x["dataset-name"]
        if not(name in per_dataset):
            per_dataset[name] = init()

        for dst in [overall, per_dataset[name]]:
            dst["total_count"] += 1
            if x["correctly-classified?"]:
                dst["correct_count"] += 1

    return {"overall": overall, "per_dataset": per_dataset}


def pretty_triplet_string(triplet):
    return "anchor={:s}, positive={:s}, negative={:s}".format(triplet["anchor"]["label"],
                                                              triplet["positive"]["label"],
                                                              triplet["negative"]["label"])


class AnalyzedResults:
    def __init__(self, correct_triplets, incorrect_triplets):
        self.correct_triplets = correct_triplets
        self.incorrect_triplets = incorrect_triplets

    def print_correct_triplets(self):
        print("correct_triplets:")
        for i in self.correct_triplets:
            print(pretty_triplet_string(i))

    def print_incorrect_triplets(self):
        print("incorrect_triplets:")
        for j in self.incorrect_triplets:
            print(pretty_triplet_string(j))


def analyze_results(dataset_results):
    correct_triplets = []
    incorrect_triplets = []

    for x in dataset_results:
        if x["correctly-classified?"]:
            correct_triplets.append(x)
        else:
            incorrect_triplets.append(x)

    return AnalyzedResults(correct_triplets, incorrect_triplets)

#def load
#def load
#lexicon = json.load()
#result = load_dataset(LocalFile("../../../datasets/occupation-name-0.csv"))
#print("First entry")
#print(result[0])







