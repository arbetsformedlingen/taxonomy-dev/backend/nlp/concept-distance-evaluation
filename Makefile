work:
	mkdir work

FORCE:

download-features:
	lein run download-features

example-triplets:
	lein run generate-triplets occupation-name-0 100 datasets/example

triplets:
	lein run generate-triplets occupation-name-0 1000 datasets/occupation-name-0

triplets-interactive:
	lein run generate-triplets-interactive occupation-name-0 datasets/occupation-name-0

normalize:
	lein run normalize datasets/occupation-name-0

py-archive:
	python3 -m pip install --upgrade build && python3 -m build
